# Base image
FROM node:18-alpine
# Create app directory

RUN mkdir -p /usr/src/app/certs

# COPY ca.pem /usr/src/app/certs/

WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
COPY prisma ./prisma/
COPY tsconfig.json ./

# Install app dependencies
RUN npm install
RUN npx prisma generate
# Bundle app source
COPY . .

EXPOSE 8080

# Creates a "dist" folder with the production build
RUN npm run build

# Start the server using the production build
CMD [ "node", "dist/main.js" ]