// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_LOCAL")
}

enum ReplyStatusEnum {
  PUBLIC
  HIDDEN
}

enum TourStatusEnum {
  DRAFT
  PUBLISHED
  HIDDEN
}

enum TourLocationTypeEnum {
  INTERNATIONAL
  DOMESTIC
}

enum ReportStatusEnum {
  PENDING
  ACCEPTED
  REJECT
}

enum ServiceTypeEnum {
  Domestic
  International
}

enum ProviderEnum {
  PROCESSING
  ACCEPTED
  REJECT
  DISABLED
  BANNED
}

enum UserStatusEnum {
  ACTIVE
  SUSPENDED
  BANNED
}

enum AvailabilityStatusEnum {
  ACTIVE
  INACTIVE
  DELETED
}

model Role {
  id        Int    @id @default(autoincrement())
  role_name String @unique @db.VarChar(10)

  //relational config

  createAt DateTime @default(now())

  Users User[]

  BlockedAccount BlockedAccount[]
}

model User {
  id               String    @id @default(uuid())
  email            String    @db.VarChar(255)
  password         String?
  phone_number     String?   @db.VarChar(20)
  full_name        String?   @db.VarChar(255)
  dob              DateTime?
  gender           String?   @db.Char(1)
  avatar_image_url String?
  banner_image_url String?

  role_id              Int
  refresh_token        String?
  refresh_token_expire DateTime?
  created_at           DateTime       @default(now())
  updated_at           DateTime       @updatedAt
  status               UserStatusEnum @default(ACTIVE)

  is_email_confirmed Boolean       @default(false)
  otp_secret         String?       @unique
  //relational config
  Providers          Provider?
  Favorites          Favorite[]
  TourReview         TourReview[]
  ReplyReview        ReviewReply[]

  // Reporter Report[] @relation("reporter")
  // Targeted Report[] @relation("targeted")

  Role Role @relation(references: [id], fields: [role_id], onDelete: Restrict)

  Order          Order[]
  ProviderReport ProviderReport[]
  Invoice        Invoice[]

  @@unique([email, role_id])
}

model Tour {
  id          String   @id @default(uuid())
  name        String?
  description String?
  footnote    String?
  // at least 1
  // tour_components  String   
  tour_images String[] @default([])
  //duration is in days,
  //stores the approximate day needed to complete the tour

  duration           Int?
  tag_id             Tag[]
  vehicle_id         Vehicle[]
  status             TourStatusEnum       @default(DRAFT)
  created_at         DateTime             @default(now())
  updated_at         DateTime             @updatedAt
  address_name       String
  address_district   String
  address_ward       String               @default("Ward")
  address_province   String
  address_country    String               @default("Viet Nam")
  tour_location_type TourLocationTypeEnum
  //relational config
  TourSchedule       TourSchedule[]
  Favorite           Favorite[]
  TourReview         TourReview[]
  Order              Order[]
  Provider           Provider?            @relation(fields: [provider_id], references: [id])
  provider_id        String?
  TicketPricing      TicketPricing[]
  TourAvailability   TourAvailability[]
}

model TourAvailability {
  id                       Int                    @id @default(autoincrement())
  tour_id                  String
  name                     String
  special_dates            Json[]
  weekdays                 Json[]
  validity_date_range_from DateTime               @default(now()) @db.Date
  validity_date_range_to   DateTime               @default(now()) @db.Date
  status                   AvailabilityStatusEnum @default(ACTIVE)
  //relational config 
  Tour                     Tour                   @relation(fields: [tour_id], references: [id], onDelete: Cascade)
}

model TourSchedule {
  id Int @id @default(autoincrement())

  tour_id            String
  title              String               @db.VarChar(255)
  description        String
  created_at         DateTime             @default(now())
  updated_at         DateTime             @updatedAt
  //relational config
  Tour               Tour                 @relation(fields: [tour_id], references: [id], onDelete: Cascade)
  TourScheduleDetail TourScheduleDetail[]
}

model TourScheduleDetail {
  id               Int          @id @default(autoincrement())
  tour_schedule_id Int
  title            String       @db.VarChar(255)
  description      String
  created_at       DateTime     @default(now())
  updated_at       DateTime     @updatedAt
  //relational config
  TourSchedule     TourSchedule @relation(fields: [tour_schedule_id], references: [id], onDelete: Cascade)
}

model Tag {
  id   Int    @id @default(autoincrement())
  name String @unique
  //relational config
  Tour Tour[]
}

model Provider {
  id               String @id @default(uuid())
  description      String
  phone            String
  email            String
  address_name     String
  address_district String
  address_ward     String
  address_province String
  address_country  String @default("Viet Nam")

  user_id          String           @unique
  // Establish the relationship with the User model
  company_name     String
  social_media     String[]         @default([])
  avatar_image_url String?
  banner_image_url String?
  service_type     ServiceTypeEnum  @default(Domestic)
  business_license String?
  tax_code         String
  status           ProviderEnum     @default(PROCESSING)
  created_at       DateTime         @default(now())
  updated_at       DateTime         @updatedAt
  //relational config
  User             User             @relation(fields: [user_id], references: [id])
  ProviderReport   ProviderReport[]
  Tour             Tour[]
}

model Favorite {
  user_id String
  tour_id String
  //relational config
  User    User   @relation(fields: [user_id], references: [id])
  Tour    Tour   @relation(fields: [tour_id], references: [id])

  @@id([user_id, tour_id])
}

model TourReview {
  id String @id @default(uuid())

  content    String
  rating     Int      @default(0)
  created_at DateTime @default(now())
  updated_at DateTime @updatedAt

  user_id       String
  tour_id       String
  //relational config
  user          User          @relation(fields: [user_id], references: [id])
  tour          Tour          @relation(fields: [tour_id], references: [id])
  ReviewReplies ReviewReply[]

  @@unique([user_id, tour_id])
  @@index([user_id, tour_id])
}

model ReviewReply {
  id           String          @id @default(uuid())
  content      String
  user_id      String
  review_id    String
  report_count Int             @default(0)
  status       ReplyStatusEnum @default(PUBLIC)

  created_at DateTime @default(now())
  updated_at DateTime @updatedAt

  user   User       @relation(fields: [user_id], references: [id])
  review TourReview @relation(fields: [review_id], references: [id])
}

model ProviderReport {
  report_type          ReportTypeEnum
  description          String?
  reporter_user_id     String
  targeted_provider_id String
  //relational config
  reporter             User             @relation(fields: [reporter_user_id], references: [id])
  provider_id          Provider         @relation(fields: [targeted_provider_id], references: [id])
  status               ReportStatusEnum @default(PENDING)

  @@id([reporter_user_id, targeted_provider_id])
}

enum ReportTypeEnum {
  UNDERPROMISE
  SCAMMER
  MISCHIEF
}

model Order {
  id            String   @id @unique @default(uuid())
  note          String?
  tour_id       String
  user_id       String
  created_at    DateTime @default(now())
  updated_at    DateTime @updatedAt
  //relational config
  BookingOnTour Tour     @relation(fields: [tour_id], references: [id])
  BookingOnUser User     @relation(fields: [user_id], references: [id])
}

model Vehicle {
  id            Int    @id @default(autoincrement())
  name          String @unique
  //relational config
  TourOnVehicle Tour[]
}

model Invoice {
  id String @id @unique @default(uuid())

  pay_date String

  user_id String

  price Decimal @db.Decimal(10, 2)

  currency String

  status String

  user User @relation(fields: [user_id], references: [id])

  url String?
}

enum BlockTypeEnum {
  REGISTER_USER
  FORGOT_PASSWORD
}

// model VerificationCode {
//   id Int @id @default(autoincrement())

//   email String
//   code  String

//   expiry DateTime

//   is_verified Boolean @default(false)

//   created_at DateTime @default(now())

//   update_at DateTime @updatedAt

//   @@unique([email, code])
// }

model BlockedAccount {
  id String @id @default(uuid())

  email String

  otpCount Int

  unblock Boolean @default(false)

  role_id Int

  type BlockTypeEnum

  Role Role @relation(fields: [role_id], references: [id])

  created_at DateTime @default(now())

  updated_at DateTime @updatedAt

  @@unique([type, email, role_id])
}

model TicketPricing {
  ticket_type_id           Int
  tour_id                  String
  pricing_type_id          Int
  minimum_booking_quantity Int?
  maximum_booking_quantity Int?
  price_range              Json[]
  //relational config
  Tour                     Tour        @relation(fields: [tour_id], references: [id])
  Ticket                   TicketType  @relation(fields: [ticket_type_id], references: [id])
  PricingType              PricingType @relation(fields: [pricing_type_id], references: [id])

  @@id([ticket_type_id, tour_id])
}

model PricingType {
  id         Int             @id @default(autoincrement())
  name       String
  //relational config
  TourTicket TicketPricing[]

  @@unique([name])
}

model TicketType {
  id         Int             @id @default(autoincrement())
  name       String
  //relational config
  TourTicket TicketPricing[]

  @@unique([name])
}
