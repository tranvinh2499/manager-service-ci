import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ROLE } from 'src/common/constants/enum';
import { Roles } from 'src/common/decorators/roles.decorator';
import { AtGuard } from 'src/common/guards';
import { RolesGuard } from 'src/common/guards/role.guard';
import { AdminService } from './admin.service';
import { CreateStaffDto } from './dto/create-staff.dto';

@Roles(ROLE.ADMIN)
@ApiBearerAuth()
@UseGuards(AtGuard, RolesGuard)
@ApiTags('Admin')
@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}
  @Post('create-staff')
  async createStaffAccount(@Body() dto: CreateStaffDto) {
    return this.adminService.createStaffAccount(dto);
  }

  // @Put('provider/:id')
  // @ApiParam({
  //   name: 'id',
  //   description: 'id of provider',
  //   required: true,
  // })
  // updateStatusProvider(
  //   @Body() dto: UpdateProviderStatusDto,
  //   @Param()
  //   param: {
  //     id: string;
  //   },
  // ) {
  //   return this.adminService.updateStatusProvider(param.id, dto);
  // }

  // @Get('customer')
  // getCustomer(@Query() dto: GetAllUserDto) {
  //   return this.adminService.getALlUser(dto);
  // }

  @Post()
  createAccount() {
    return this.adminService.createAccount();
  }
}
