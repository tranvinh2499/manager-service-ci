import { Module } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { DataAccessModule } from '../data-access/data-access.module';

@Module({
  controllers: [AdminController],
  providers: [AdminService],
  imports: [DataAccessModule],
})
export class AdminModule {}
