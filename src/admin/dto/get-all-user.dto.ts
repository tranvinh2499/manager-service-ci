import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsInt, IsOptional, IsString } from 'class-validator';
import { UserOrderBy } from '../../common/models/user';

export class GetAllUserDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  page?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  limit?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(UserOrderBy)
  orderBy?: string;
}
