import {
  BadRequestException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Provider } from '@prisma/client';
import { ROLE } from '../common/constants/enum';

import { ResponseException } from 'src/common/interface/exception.interface';
import { ResponseDto } from 'src/common/models/interface';
import { GetUserPaginationDto } from 'src/user/dto/get-user-pagination.dto';
import { ProviderStatus } from '../common/models/provider';
import { hashData } from '../common/utils';
import { ProviderDataAccess } from '../data-access/provider-access.service';
import { UserDataAccess } from '../data-access/user-access.service';
import { PrismaService } from '../prisma/prisma.service';
import { UpdateProviderStatusDto } from '../provider/dto/update-provider-status.dto';
import { CreateStaffDto } from './dto/create-staff.dto';

@Injectable()
export class AdminService {
  constructor(
    private readonly providerAccess: ProviderDataAccess,
    private readonly userDataAccess: UserDataAccess,
    private readonly prisma: PrismaService,
  ) {}

  async updateStatusProvider(
    providerId: string,
    dto: UpdateProviderStatusDto,
  ): Promise<ResponseDto<Provider>> {
    const check = await this.providerAccess.getProviderById(providerId);
    if (!check) {
      throw new NotFoundException('Provider not found or have been accepted');
    }
    if (check.status === ProviderStatus.ACCEPTED) {
      throw new BadRequestException('provider is already accepted');
    }
    if (check.status === ProviderStatus.REJECT) {
      throw new BadRequestException('provider is rejected');
    }
    try {
      const provider = await this.providerAccess.updateProviderStatus(
        providerId,
        dto.status,
      );
      return {
        status: HttpStatus.OK,
        message: `update provider status successfully`,
        data: provider,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getALlUser(dto: GetUserPaginationDto): Promise<{
    total: number;
    Users: any;
  }> {
    return await this.userDataAccess.getAllUsers(dto);
  }

  async createAccount() {
    const hash = await hashData('admin');
    return await this.prisma.user.create({
      data: {
        email: 'admin@admin.com',
        password: hash,
        role_id: ROLE.ADMIN,
      },
    });
  }

  async createStaffAccount(dto: CreateStaffDto) {
    const account = `${dto.lastName}${dto.firstName.charAt(0)}${
      dto?.middleName?.charAt(0) || ''
    }`;
    console.log('file: admin.service.ts:81 ~ account ~ account:', account);

    // Find all emails that contain the account name
    const emails = await this.prisma.user.findMany({
      where: {
        email: {
          contains: account,
          mode: 'insensitive',
        },
      },
    });

    // Check if any emails were found
    if (emails.length > 0) {
      // Extract and compare the indexes to find the highest
      const highestIndex = this.findHighestIndex(
        emails.map(user => this.getEmailIndex(user.email)),
      );

      // Increment the highest index
      const newIndex = highestIndex + 1;

      // Create a new email address with the incremented index
      const newEmail = `${account}${newIndex}@zest.trip.com`.toLowerCase();

      const staff = await this.createStaff(newEmail);
      return {
        status: HttpStatus.CREATED,
        message: 'Create staff account successfully.',
        data: staff,
      };
    } else {
      // If no emails were found, create the first email with index 1
      const initialEmail = `${account}1@zest.trip.com`.toLowerCase();

      try {
        const staff = await this.createStaff(initialEmail);
        return {
          status: HttpStatus.CREATED,
          message: 'Create staff account successfully.',
          data: staff,
        };
      } catch (error) {
        if (error.code === 'P2002')
          throw new ResponseException({
            errorCode: error.code,
            status: HttpStatus.BAD_REQUEST,
            message: 'this account have been exist in system',
          });

        throw new ResponseException({
          errorCode: error.code,
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'error when create staff account',
        });
      }
    }
  }

  private async createStaff(email: string) {
    const hashPassword = await hashData('123');
    const staff = await this.userDataAccess.createStaffUser(
      email,
      hashPassword,
    );
    delete staff.password;

    return { ...staff, password: '123' };
  }
  // Helper function to extract the index from an email address
  private getEmailIndex(email: string): number {
    const regex = /([a-zA-Z]+)(\d+)/;
    const match = email.match(regex);
    if (match) {
      return parseInt(match[2], 10);
    } else {
      return 0; // Default to 0 if no index is found
    }
  }

  // Helper function to find the highest index in an array of numbers
  private findHighestIndex(indexes: number[]): number {
    return Math.max(...indexes);
  }
}
