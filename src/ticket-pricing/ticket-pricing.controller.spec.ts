import { Test, TestingModule } from '@nestjs/testing';
import { TicketPricingController } from './ticket-pricing.controller';
import { TicketPricingService } from './ticket-pricing.service';
import { UtilitiesModule } from 'src/utilities/utilities.module';
import { DataAccessModule } from 'src/data-access/data-access.module';

describe('TicketPricingController', () => {
  let controller: TicketPricingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TicketPricingController],
      providers: [TicketPricingService],
      imports: [UtilitiesModule, DataAccessModule],
    }).compile();

    controller = module.get<TicketPricingController>(TicketPricingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
