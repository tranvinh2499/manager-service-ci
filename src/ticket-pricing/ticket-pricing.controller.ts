import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { TicketPricingService } from './ticket-pricing.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/common/decorators/roles.decorator';
import { AtGuard } from 'src/common/guards';
import { RolesGuard } from 'src/common/guards/role.guard';

import {
  CreateTicketPricingDTO,
  GetByTourIdDTO,
} from './dto/create-pricing.dto';
import { GetCurrentUser } from 'src/common/decorators/get-current-user.decorator';
import { ResponseServer } from 'src/common/interface/response.interface';
import { ROLE } from 'src/common/constants/enum';

@ApiTags('Ticket Pricing')
@Roles(ROLE.PROVIDER)
@UseGuards(AtGuard, RolesGuard)
@ApiBearerAuth()
@Controller('pricing')
export class TicketPricingController {
  constructor(private readonly ticketPricingService: TicketPricingService) {}

  @Post()
  async createTicketPricing(
    @Body() createTicketPricingDTO: CreateTicketPricingDTO,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    return new ResponseServer({
      status: HttpStatus.CREATED,
      message: 'Created Ticked Pricing',
      data: await this.ticketPricingService.createPricing(
        createTicketPricingDTO,
        providerUserID,
      ),
    });
  }
  @Get('/byTour/:tour_id')
  async getOneById(@Param() dto: GetByTourIdDTO) {
    return new ResponseServer({
      status: HttpStatus.OK,
      message: 'Get Pricing success',
      data: await this.ticketPricingService.getByTourId(dto.tour_id),
    });
  }
  @Put('/update')
  async updateByTourId(
    @Body() dto: CreateTicketPricingDTO,
    @GetCurrentUser('id') providerUserID: string,
  ) {
    return new ResponseServer({
      status: HttpStatus.I_AM_A_TEAPOT,
      message: 'Updated Ticked Pricing',
      data: await this.ticketPricingService.updateByTourId(dto, providerUserID),
    });
  }
}
