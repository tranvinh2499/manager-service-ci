import { Module } from '@nestjs/common';
import { TicketPricingService } from './ticket-pricing.service';
import { TicketPricingController } from './ticket-pricing.controller';
import { UtilitiesModule } from 'src/utilities/utilities.module';
import { DataAccessModule } from 'src/data-access/data-access.module';

@Module({
  controllers: [TicketPricingController],
  providers: [TicketPricingService],
  imports: [UtilitiesModule, DataAccessModule],
})
export class TicketPricingModule {}
