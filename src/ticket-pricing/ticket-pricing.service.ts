import { TicketPricingAccessService } from './../data-access/ticket-pricing-access.service';
import { Injectable } from '@nestjs/common';
import { CreateTicketPricingDTO, PricingData } from './dto/create-pricing.dto';
import { ValidationService } from 'src/utilities/validation.service';
import { ResponseException } from 'src/common/interface/exception.interface';
import {
  PriceRange,
  Ticket,
  TicketMapping,
} from 'src/common/models/ticket-pricing';

@Injectable()
export class TicketPricingService {
  constructor(
    private ticketPricingAccessService: TicketPricingAccessService,
    private validationService: ValidationService,
  ) {}
  async createPricing(dto: CreateTicketPricingDTO, puid: string) {
    //todo check number of ticket data in the array of data front end sends, if number > 2 then throw
    const currentPricing =
      await this.ticketPricingAccessService.getManyByTourId(dto.tour_id);
    if (currentPricing.length >= 1) {
      throw new ResponseException({
        status: 400,
        message: 'Pricing is already created for this tour',
      });
    }
    //can not create a pricing without the default Adult ticket
    if (!dto.pricing_data.find(item => item.ticket_type === Ticket.ADULT)) {
      throw new ResponseException({
        status: 400,
        message: 'Pricing data need to have ADULT pricing',
      });
    }

    // if increase the types of ticket in the system, should increase the number 2 to max type
    if (
      currentPricing.find(
        item => item.ticket_type_id === TicketMapping.ADULT,
      ) &&
      dto.pricing_data.length === 2
    ) {
      dto.pricing_data = dto.pricing_data.filter(
        item => item.ticket_type !== Ticket.ADULT,
      );
    }
    await this.isPricingValid(dto, puid);
    return this.ticketPricingAccessService.createManyPricing(dto);
  }

  async getByTourId(tourId: string) {
    return await this.ticketPricingAccessService.getManyByTourId(tourId);
  }

  async updateByTourId(dto: CreateTicketPricingDTO, puid: string) {
    const currentPricing =
      await this.ticketPricingAccessService.getManyByTourId(dto.tour_id);
    if (currentPricing.length < 1) {
      throw new ResponseException({
        status: 400,
        message: 'Pricing has not been created, can not update',
      });
    }
    const newData = await this.isPricingValid(dto, puid);
    return await this.ticketPricingAccessService.updatePricingByTourId(newData);
  }

  sortPriceRange(price_range: PriceRange[]): PriceRange[] {
    return price_range.sort((item_1, item_2) => {
      return item_1.from_amount - item_2.from_amount;
    });
  }
  validatePriceRange(priceRange: PriceRange[]) {
    console.log(priceRange);
    if (priceRange && priceRange.length > 1) {
      // to length - 1 because each iteration is checking i and i + 1
      for (let i = 0; i < priceRange.length - 1; i++) {
        const first = priceRange[i];
        const second = priceRange[i + 1];
        if (i === 0 && first.from_amount !== 0) {
          throw new ResponseException({
            status: 400,
            message: 'Invalid pricing range, pricing must start with 0',
          });
        }
        const isValidPricingRange =
          first.from_amount <= first.to_amount &&
          first.to_amount - second.from_amount === -1;

        if (
          i === priceRange.length - 2 &&
          second.from_amount > second.to_amount
        ) {
          throw new ResponseException({
            status: 400,
            message: 'Invalid pricing range, from can not be higher than to',
          });
        }
        if (!isValidPricingRange) {
          throw new ResponseException({
            status: 400,
            message: 'Invalid pricing range',
          });
        }
      }
    } else {
      if (
        priceRange[0].from_amount > priceRange[0].to_amount ||
        priceRange[0].from_amount !== 0
      ) {
        throw new ResponseException({
          status: 400,
          message: 'Invalid pricing range',
        });
      }
    }
  }

  async processTicketPricing(element: PricingData): Promise<void> {
    const sortedPrice = this.sortPriceRange(element.price_range);
    this.validatePriceRange(sortedPrice);
    if (
      element.maximum_booking_quantity &&
      element.minimum_booking_quantity &&
      element.maximum_booking_quantity < element.minimum_booking_quantity
    ) {
      throw new ResponseException({
        status: 400,
        error:
          'Maximum booking quantity must be higher or equal to the minimum booking quantity',
      });
    }
    if (
      element.maximum_booking_quantity &&
      element.maximum_booking_quantity >
        sortedPrice[sortedPrice.length - 1].to_amount
    ) {
      throw new ResponseException({
        status: 400,
        message:
          'Maximum booking quantity must not exceed max number of ticket',
      });
    }
    if (
      element.minimum_booking_quantity &&
      element.minimum_booking_quantity >
        sortedPrice[sortedPrice.length - 1].to_amount
    ) {
      throw new ResponseException({
        status: 400,
        message:
          'Minimum booking quantity must not exceed max number of ticket',
      });
    }
    element.price_range = sortedPrice;
  }
  async isPricingValid(
    dto: CreateTicketPricingDTO,
    puid: string,
  ): Promise<CreateTicketPricingDTO> {
    await this.validationService.doesProviderHasTourOrThrow(dto.tour_id, puid);
    for (const element of dto.pricing_data) {
      await this.processTicketPricing(element);
    }
    return dto;
  }
}
