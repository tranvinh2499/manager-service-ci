import { Test, TestingModule } from '@nestjs/testing';
import { TicketPricingService } from './ticket-pricing.service';

describe('TicketPricingService', () => {
  let service: TicketPricingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TicketPricingService],
    }).compile();

    service = module.get<TicketPricingService>(TicketPricingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
