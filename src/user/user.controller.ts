import {
  Body,
  Controller,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  ParseFilePipe,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { GetCurrentUser } from '../common/decorators/get-current-user.decorator';
import { AtGuard } from '../common/guards';
import { UserService } from './user.service';

import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiProperty,
  ApiTags,
} from '@nestjs/swagger';
import { UpdateUserDto } from './dto/update-user.dto';
const MAX_FILE_SIZE = 1024 * 1024 * 8;
const IMAGE_FILE_TYPE = /(jpg|jpeg|png|webp)$/;
class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
}

@ApiTags('User')
@Controller('users')
@UseGuards(AtGuard)
@ApiBearerAuth()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('me')
  getProfile(@GetCurrentUser('id') userId: string) {
    return this.userService.getProfile(userId);
  }

  @Put('me/update')
  updateProfile(
    @GetCurrentUser('id') userId: string,
    @Body() dto: UpdateUserDto,
  ) {
    return this.userService.updateProfile(userId, dto);
  }

  @Put('me/avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'file type jpg/jpeg/png',
    type: FileUploadDto,
  })
  updateAvatar(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
    @GetCurrentUser('id') userId: string,
  ) {
    return this.userService.updateAvatar(file, userId);
  }
  @Put('me/banner')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'file type jpg/jpeg/png',
    type: FileUploadDto,
  })
  updateBannerImage(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
    @GetCurrentUser('id') userId: string,
  ) {
    return this.userService.updateBannerImage(file, userId);
  }
}
