import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';
import { PaginationDto } from 'src/common/models/common';
import { UserOrderBy } from 'src/common/models/user';

export class GetUserPaginationDto extends PaginationDto {
  @ApiPropertyOptional()
  @IsEnum(UserOrderBy)
  orderBy?: string;
}
