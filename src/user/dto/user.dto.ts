import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDate,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class UserDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  email?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  phoneNumber?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  fullName?: string;

  @ApiPropertyOptional()
  @IsOptional()
  dob?: Date;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  gender?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  avatarImageUrl?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  bannerImageUrl?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  status?: number;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  roleId?: number;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  rtHash?: string;

  @IsDate()
  @IsOptional()
  rtExpired?: Date;

  @IsInt()
  @IsOptional()
  reportCount?: number;
}
