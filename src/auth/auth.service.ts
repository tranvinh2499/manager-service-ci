import {
  BadRequestException,
  ForbiddenException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { BlockTypeEnum, User } from '@prisma/client';
import * as argon from 'argon2';
import jwtDecode from 'jwt-decode';
import { ResponseDto } from 'src/common/models/interface';
import { Tokens } from '../auth/types/token.type';
import { ROLE } from '../common/constants/enum';
import { hashData } from '../common/utils';
import { UserDataAccess } from '../data-access/user-access.service';
import { ValidationService } from '../utilities/validation.service';
import { LoginGoogleDto } from './dto/login-google.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';
import { OtpService } from './otp.service';

type GoogleUser = {
  email: string;
  name: string;
  picture: string;
};

@Injectable()
export class AuthService {
  constructor(
    private readonly jwt: JwtService,
    private readonly userDataAccess: UserDataAccess,
    private readonly config: ConfigService,
    private readonly otpService: OtpService,
    private readonly validationService: ValidationService,
  ) {}

  async signIn(dto: SignInDto): Promise<ResponseDto<Tokens>> {
    await this.validationService.isEmailBlockedOrThrow(dto.email);
    const user = await this.userDataAccess.getUserByEmailAndRole(
      dto.email,
      ROLE.PROVIDER,
    );

    if (!user) throw new ForbiddenException('User not exist');

    if (user && user.is_email_confirmed === false)
      throw new BadRequestException('Email is not verified yet');

    const passwordMatches = await argon.verify(user.password, dto.password);

    if (!passwordMatches) throw new ForbiddenException('Password incorrect');
    await this.validationService.isProviderAcceptedOrThrow(user.id);
    try {
      const token = await this.getTokens(user);

      await this.updateRtHash(user.id, token.refresh_token);

      return {
        status: HttpStatus.OK,
        message: 'sign in successfully',
        data: token,
      };
    } catch (error) {
      if (error instanceof ForbiddenException) {
        throw new BadRequestException({
          status: HttpStatus.BAD_REQUEST,
          message: 'Bad request: ' + error.message,
        });
      } else {
        throw new InternalServerErrorException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'error when sign in',
          error: error,
        });
      }
    }
  }
  async signUp(dto: SignUpDto): Promise<ResponseDto<Tokens>> {
    const hash = await hashData(dto.password);
    const user = await this.userDataAccess.getUserByEmailAndRole(
      dto.email,
      ROLE.PROVIDER,
    );
    // user must verify email first
    if (!user) throw new BadRequestException('Email has not been verified');
    if (user && user.is_email_confirmed === true)
      throw new BadRequestException('User exist');
    //if (user && user.is_email_confirmed === false)
    await this.otpService.verifyOtp(
      dto.email,
      dto.otp,
      BlockTypeEnum.REGISTER_USER,
      user.otp_secret,
    );
    try {
      // update user password
      await this.userDataAccess.updateUserPassword(user.id, hash);
      const _user = await this.userDataAccess.updateUserEmailConfirmed(user.id);

      delete _user.password;
      delete _user.refresh_token;

      const tokens = await this.getTokens(_user);
      return {
        status: HttpStatus.CREATED,
        message: 'signup successfully',
        data: tokens,
      };
    } catch (error) {
      console.log(error);
      if (error.code === 'P2002') throw new ForbiddenException('user existed');
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when signup',
        error,
      });
    }
  }

  async googleLogin(dto: LoginGoogleDto): Promise<ResponseDto<Tokens>> {
    try {
      const decode: GoogleUser = jwtDecode(dto.accessToken);

      const user = await this.userDataAccess.getUniqueUser({
        email: decode.email,
        roleId: ROLE.PROVIDER,
      });

      if (!user) {
        const hash = await hashData('google-login');

        const newUser = await this.userDataAccess.createUser({
          email: decode.email,
          password: hash,
          avatarImageUrl: decode.picture,
          fullName: decode.name,
        });

        await this.userDataAccess.updateUserEmailConfirmed(newUser.id);

        const token = await this.getTokens(newUser);
        await this.updateRtHash(newUser.id, token.refresh_token);
        return {
          status: HttpStatus.OK,
          message: 'signup successfully',
          data: token,
        };
      }
      const token = await this.getTokens(user);
      await this.updateRtHash(user.id, token.refresh_token);
      return {
        status: HttpStatus.OK,
        message: 'sign in successfully',
        data: token,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'something happen',
        error,
      });
    }
  }

  async refresh(userId: string, rt: string): Promise<ResponseDto<Tokens>> {
    try {
      const user = await this.userDataAccess.getUniqueUserById(userId);

      if (!user) throw new ForbiddenException('Access denied');

      const rtMatches = argon.verify(user.refresh_token, rt);

      if (!rtMatches) throw new ForbiddenException('Access denied');

      const tokens = await this.getTokens(user);

      await this.updateRtHash(userId, tokens.refresh_token);

      return {
        status: HttpStatus.OK,
        message: 'refresh access token success',
        data: tokens,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when refresh token',
        error: error.response,
      });
    }
  }

  async logout(userId: string) {
    try {
      await this.userDataAccess.deleteUserRtHash(userId);
    } catch (error) {
      throw new InternalServerErrorException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'error when logout',
        error: error.response,
      });
    }
  }

  async forgotPassword(dto: ResetPasswordDto) {
    const user = await this.userDataAccess.getUserByEmailAndRole(
      dto.email,
      ROLE.PROVIDER,
    );

    if (!user) throw new NotFoundException('Email not exist');

    await this.otpService.verifyOtp(
      dto.email,
      dto.otp,
      BlockTypeEnum.FORGOT_PASSWORD,
      user.otp_secret,
    );
    const hash = await hashData(dto.newPassword);
    try {
      await this.userDataAccess.updateUserPassword(user.id, hash);
      return {
        status: HttpStatus.OK,
        message: 'Your password change successfully',
      };
    } catch (error) {
      return error;
    }
  }

  async getTokens(user: User) {
    const [at, rt] = await Promise.all([
      this.jwt.signAsync(
        {
          id: user.id,
          email: user.email,
          phone_number: user.phone_number,
          full_name: user.full_name,
          dob: user.dob,
          gender: user.gender,
          avatar_image_url: user.avatar_image_url,
          banner_image_url: user.banner_image_url,
          status: user.status,
          role_id: user.role_id,
        },
        {
          secret: this.config.get('JWT_AT_SECRET'),
          expiresIn: '30m',
        },
      ),
      this.jwt.signAsync(
        {
          id: user.id,
          email: user.email,
          phone_number: user.phone_number,
          full_name: user.full_name,
          dob: user.dob,
          gender: user.gender,
          avatar_image_url: user.banner_image_url,
          banner_image_url: user.banner_image_url,
          status: user.status,
          role_id: user.role_id,
        },
        {
          secret: this.config.get('JWT_RT_SECRET'),
          expiresIn: '30d',
        },
      ),
    ]);
    return {
      access_token: at,
      refresh_token: rt,
    };
  }

  async updateRtHash(userId: string, rt: string) {
    const hash = await hashData(rt);

    await this.userDataAccess.updateUserById(userId, {
      rtHash: hash,
    });
    const currentDate = new Date();

    const futureDate = new Date(currentDate);

    futureDate.setDate(currentDate.getDate() + 30);

    await this.userDataAccess.updateUserById(userId, {
      rtExpired: futureDate,
    });
  }

  async signInManager(dto: SignInDto) {
    //* STAFF
    const staff = await this.userDataAccess.getUserByEmailAndRole(
      dto.email,
      ROLE.STAFF,
    );

    if (staff && staff !== null) {
      const passwordMatches = await argon.verify(staff.password, dto.password);
      if (!passwordMatches) throw new ForbiddenException('Password incorrect');
      const token = await this.getTokens(staff);
      return token;
    }
    //*ADMIN
    const admin = await this.userDataAccess.getUserByEmailAndRole(
      dto.email,
      ROLE.ADMIN,
    );

    if (admin && admin !== null) {
      const passwordMatches = await argon.verify(admin.password, dto.password);

      if (!passwordMatches) throw new ForbiddenException('Password incorrect');
      const token = await this.getTokens(admin);
      return token;
    }
  }
}
