import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { OtpService } from './otp.service';
import { CreateOtpDTO } from './dto/create-otp.dto';

@ApiTags('Auth')
@Controller('otp')
export class OtpController {
  constructor(private readonly otpService: OtpService) {}

  @Post('generate')
  getOtp(@Body() body: CreateOtpDTO) {
    return this.otpService.getOtp(body.email, body.type);
  }

  // @Post('verify')
  // @HttpCode(HttpStatus.OK)
  // verifyOtp(@Body() body: VerifyOtpDTO) {
  //   return this.otpService.verifyOtp(body.email, body.otp, body.type);
  // }
}
