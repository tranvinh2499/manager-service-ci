import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';

import { Request } from 'express';
import { ResponseServer } from 'src/common/interface/response.interface';
import { AuthService } from './auth.service';
import { LoginGoogleDto } from './dto/login-google.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('signin')
  singIn(@Body() dto: SignInDto) {
    return this.authService.signIn(dto);
  }

  @Post('signup')
  async signUp(@Body() dto: SignUpDto) {
    const data = await this.authService.signUp(dto);
    return data;
  }

  @Post('signout')
  @UseGuards(AuthGuard('jwt'))
  logout(@Req() req: Request) {
    return this.authService.logout(req.user['id']);
  }

  @UseGuards(AuthGuard('jwt-refresh'))
  @Post('refresh')
  refresh(@Req() req: Request) {
    return this.authService.refresh(req.user['id'], req.user['refreshToken']);
  }

  @Post('google-login')
  googleLogin(@Body() dto: LoginGoogleDto) {
    return this.authService.googleLogin(dto);
  }

  @Post('forget-password')
  forgetPassword(@Body() body: ResetPasswordDto) {
    return this.authService.forgotPassword(body);
  }

  @Post('manager/signin')
  @HttpCode(HttpStatus.OK)
  async signInManager(@Body() dto: SignInDto) {
    return new ResponseServer({
      status: HttpStatus.OK,
      message: 'manager sign in ',
      data: await this.authService.signInManager(dto),
    });
  }
}
