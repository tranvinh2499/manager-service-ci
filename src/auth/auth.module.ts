import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';

import { JwtModule } from '@nestjs/jwt';
import { DataAccessModule } from '../data-access/data-access.module';
import { OtpController } from './otp.controller';
import { OtpService } from './otp.service';
import { UtilitiesModule } from '../utilities/utilities.module';
import { RtStrategy } from './strategies/rt.strategy';
import { AtStrategy } from './strategies/at.strategy';

@Module({
  imports: [JwtModule.register({}), DataAccessModule, UtilitiesModule],
  controllers: [AuthController, OtpController],
  providers: [AuthService, RtStrategy, AtStrategy, OtpService],
})
export class AuthModule {}
