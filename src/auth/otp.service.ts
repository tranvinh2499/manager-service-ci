import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { BlockTypeEnum } from '@prisma/client';
import * as speakeasy from 'speakeasy';
import { OTP_DURATION } from '../common/constants/constant';
import { OTP_RETRY_LIMIT } from '../common/constants/enum';
import { BLOCK_ACCOUNT_OTP } from '../common/constants/string';
import { BlockedAccountDataService } from '../data-access/blocked-account-access.service';
import { UserDataAccess } from '../data-access/user-access.service';
import { MailService } from '../utilities/mail.service';

@Injectable()
export class OtpService {
  constructor(
    private readonly blockedAccountService: BlockedAccountDataService,
    private readonly userDataAccess: UserDataAccess,
    private readonly mailService: MailService,
  ) {}

  // api generate otp
  async getOtp(email: string, type: string) {
    const _user = await this.userDataAccess.getUserByEmail(email);
    const blockType = BlockTypeEnum[type as keyof BlockTypeEnum];

    let _blockedAccount =
      await this.blockedAccountService.getBlockedAccountByEmailAndType(
        email,
        blockType,
      );
    // check if blocked account not exist
    if (
      !_blockedAccount ||
      _blockedAccount === null ||
      _blockedAccount === undefined
    ) {
      _blockedAccount = await this.blockedAccountService.createOne(
        email,
        blockType,
      );
    }

    // if blocked account exist and otp count over the limit
    if (
      _blockedAccount &&
      _blockedAccount.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT
    ) {
      throw new BadRequestException(BLOCK_ACCOUNT_OTP(email));
    }
    //! FORGOT_PASSWORD
    if (type === BlockTypeEnum.FORGOT_PASSWORD) {
      //check if user exist
      if (!_user) throw new BadRequestException('User not exist');
      const secret = _user.otp_secret;
      const code = this.generateOtpCode(secret, 600);
      await this.blockedAccountService.updateOtpCount(_blockedAccount.id);
      await this.mailService.sendMail(email, code);
      return { status: 201, message: `your otp code is ${code}` };
    }

    //! REGISTER
    const secret = this.generateSecret();
    if (_user && _user.is_email_confirmed === true)
      throw new BadRequestException('User exist');

    if (_user && _user.is_email_confirmed === false) {
      const code = this.generateOtpCode(_user.otp_secret);
      await this.blockedAccountService.updateOtpCount(_blockedAccount.id);
      await this.mailService.sendMail(email, code);
      return { status: 201, message: `your otp code is ${code}` };
    } else {
      await this.userDataAccess.createNewProviderUser(email, secret.base32);
      try {
        //* create new user if first user
        const code = this.generateOtpCode(secret.base32);
        await this.mailService.sendMail(email, code);

        return { status: 201, message: `your otp code is ${code}` };
      } catch (error) {
        return new InternalServerErrorException(error);
      }
    }
  }

  async verifyOtp(
    email: string,
    otp: string,
    type: string,
    secret: string,
  ): Promise<void> {
    const blockType = BlockTypeEnum[type as keyof BlockTypeEnum];

    const blockedAccount =
      await this.blockedAccountService.getBlockedAccountByEmailAndType(
        email,
        blockType,
      );
    // no need to check null due to create in getOtp method

    const check = this.verifyCode(secret, otp);

    if (check) {
      await this.blockedAccountService.updateBlockedAccount(blockedAccount.id, {
        unblock: true,
        otpCount: 0,
      });
      console.log('verified success');
    } else {
      console.log('error at otp.service.ts line 100');
      throw new BadRequestException('Otp Invalid');
    }
  }

  private generateOtpCode(secret: string, duration?: number) {
    return speakeasy.totp({
      secret: secret,
      encoding: 'base32',
      step: duration || OTP_DURATION,
    });
  }

  private verifyCode(secret: string, token: string): boolean {
    return speakeasy.totp.verify({
      secret: secret,
      encoding: 'base32',
      token: token,
      step: 180,
    });
  }
  private generateSecret() {
    return speakeasy.generateSecret({
      length: 20,
    });
  }
}
