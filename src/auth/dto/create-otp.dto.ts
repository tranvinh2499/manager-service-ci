import { ApiProperty } from '@nestjs/swagger';
import { BlockTypeEnum } from '@prisma/client';
import { IsEmail, IsEnum, IsNotEmpty } from 'class-validator';

export class CreateOtpDTO {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;
  @IsNotEmpty()
  @IsEnum(BlockTypeEnum)
  @ApiProperty({
    enum: BlockTypeEnum,
  })
  type: string;
}
