import { Module } from '@nestjs/common';
import { FirebaseService } from 'src/utilities/firebase.service';
import { AvailabilityAccessService } from './availability-access.service';
import { BlockedAccountDataService } from './blocked-account-access.service';
import { DataAccessService } from './data-access.service';
import { ProviderDataAccess } from './provider-access.service';
import { TagAccessService } from './tag-access.service';
import { TourAccessService } from './tour-access.service';
import { UserDataAccess } from './user-access.service';
import { VehicleAccessService } from './vehicle-access.service';
import { VerificationCodeDataAccessService } from './verification-code-access.service';
import { TicketPricingAccessService } from './ticket-pricing-access.service';

@Module({
  providers: [
    UserDataAccess,
    ProviderDataAccess,
    TourAccessService,
    TagAccessService,
    VehicleAccessService,
    FirebaseService,
    VerificationCodeDataAccessService,
    BlockedAccountDataService,
    AvailabilityAccessService,
    DataAccessService,
    TicketPricingAccessService,
  ],
  exports: [
    FirebaseService,
    UserDataAccess,
    ProviderDataAccess,
    TourAccessService,
    TagAccessService,
    VehicleAccessService,
    VerificationCodeDataAccessService,
    BlockedAccountDataService,
    AvailabilityAccessService,
    DataAccessService,
    TicketPricingAccessService,
  ],
})
export class DataAccessModule {}
