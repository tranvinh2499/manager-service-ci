import {
  PricingTypeMapping,
  TicketMapping,
} from './../common/models/ticket-pricing';
import { HttpStatus, Injectable } from '@nestjs/common';

import { PrismaService } from '../prisma/prisma.service';
import { CreateTicketPricingDTO } from 'src/ticket-pricing/dto/create-pricing.dto';
import { ResponseException } from 'src/common/interface/exception.interface';
import { Prisma, TicketPricing } from '@prisma/client';
@Injectable()
export class TicketPricingAccessService {
  constructor(private prisma: PrismaService) {}
  async getManyByTourId(tourId: string): Promise<TicketPricing[]> {
    try {
      return await this.prisma.ticketPricing.findMany({
        where: {
          tour_id: tourId,
        },
        include: {
          Ticket: { select: { name: true } },
          PricingType: { select: { name: true } },
        },
      });
    } catch (error) {
      throw new ResponseException({ status: 500, message: error });
    }
  }
  async createManyPricing(dto: CreateTicketPricingDTO): Promise<any> {
    try {
      const success = await this.prisma.ticketPricing.createMany({
        data: dto.pricing_data.map(params => {
          return {
            pricing_type_id: PricingTypeMapping[params.pricing_type],
            tour_id: dto.tour_id,
            ticket_type_id: TicketMapping[params.ticket_type],
            price_range: params.price_range as unknown as Prisma.JsonArray,
            maximum_booking_quantity: params.maximum_booking_quantity,
            minimum_booking_quantity: params.minimum_booking_quantity,
          };
        }),
      });
      if (success) {
        return await this.prisma.ticketPricing.findMany({
          where: { tour_id: dto.tour_id },
          include: {
            Ticket: { select: { name: true } },
            PricingType: { select: { name: true } },
          },
        });
      }
      return false;
    } catch (error) {
      console.log(error);
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error,
      });
    }
  }

  async updatePricingByTourId(dto: CreateTicketPricingDTO): Promise<any> {
    try {
      for (const element of dto.pricing_data) {
        await this.prisma.ticketPricing.upsert({
          where: {
            ticket_type_id_tour_id: {
              ticket_type_id: TicketMapping[element.ticket_type],
              tour_id: dto.tour_id,
            },
          },
          create: {
            pricing_type_id: PricingTypeMapping[element.pricing_type],
            tour_id: dto.tour_id,
            ticket_type_id: TicketMapping[element.ticket_type],
            price_range: element.price_range as unknown as Prisma.JsonArray,
            maximum_booking_quantity: element.maximum_booking_quantity,
            minimum_booking_quantity: element.minimum_booking_quantity,
          },
          update: {
            pricing_type_id: PricingTypeMapping[element.pricing_type],
            tour_id: dto.tour_id,
            ticket_type_id: TicketMapping[element.ticket_type],
            price_range: element.price_range as unknown as Prisma.JsonArray,
            maximum_booking_quantity: element.maximum_booking_quantity,
            minimum_booking_quantity: element.minimum_booking_quantity,
          },
        });
      }
      return await this.prisma.ticketPricing.findMany({
        where: { tour_id: dto.tour_id },
        include: {
          Ticket: { select: { name: true } },
          PricingType: { select: { name: true } },
        },
      });
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Internal server error',
        error: error,
      });
    }
  }
}
