import { HttpStatus, Injectable } from '@nestjs/common';
import { Prisma, Tour } from '@prisma/client';

import { ResponseException } from 'src/common/interface/exception.interface';
import { PrismaService } from '../prisma/prisma.service';
import { CreateOneTourDTO } from '../tour/dto/create-one-tour.dto';
import { GetAllTourDTO } from '../tour/dto/get-all-tour.dto';

@Injectable()
export class TourAccessService {
  constructor(private prisma: PrismaService) {}

  async getOne(tourId: string): Promise<Tour> {
    const tour = await this.prisma.tour.findUnique({
      where: {
        id: tourId,
      },
      include: {
        Provider: { include: { User: true } },
        TourAvailability: true,
        TourReview: { include: { ReviewReplies: true } },
        TourSchedule: { include: { TourScheduleDetail: true } },
        vehicle_id: true,
        tag_id: true,
        TicketPricing: { include: { PricingType: true, Ticket: true } },
      },
    });

    return tour;
  }

  async getOneWithProvider(tourId: string): Promise<Tour> {
    const tour = await this.prisma.tour.findUnique({
      where: {
        id: tourId,
      },
      include: {
        Provider: { include: { User: true } },
        TourAvailability: true,
        TourReview: { include: { ReviewReplies: true } },
        TourSchedule: { include: { TourScheduleDetail: true } },
        vehicle_id: true,
        tag_id: true,
      },
    });

    return tour;
  }

  async createOne(tourDTO: CreateOneTourDTO, UID: string): Promise<Tour> {
    try {
      // const getTag = await
      console.log(tourDTO.TourSchedule[2]);
      console.log('____________');
      const scheduleData = tourDTO.TourSchedule.map(schedule => {
        if (schedule?.schedule_detail) {
          return {
            title: schedule.title,
            description: schedule.description,
            TourScheduleDetail: {
              create: schedule.schedule_detail.map(detail => ({
                title: detail.title,
                description: detail.description,
              })),
            },
          };
        } else {
          return {
            title: schedule.title,
            description: schedule.description,
          };
        }
      });
      console.log('____________________');
      console.log(scheduleData);
      console.log('____________________');

      const data = await this.prisma.tour.create({
        data: {
          provider_id: UID,
          name: tourDTO.name,
          description: tourDTO.description,
          footnote: tourDTO.footnote,
          tour_images: tourDTO?.tour_images,
          duration: tourDTO.duration,

          tag_id: {
            connect: tourDTO.tag_id.map(tagId => ({ id: tagId })),
          },
          vehicle_id: {
            connect: tourDTO.vehicle_id.map(vehicleId => ({ id: vehicleId })),
          },
          address_name: tourDTO.address_name,
          address_district: tourDTO.address_district,
          address_ward: tourDTO.address_ward,
          address_province: tourDTO.address_province,
          address_country: tourDTO.address_country,
          tour_location_type: tourDTO.tour_location_type,
          TourSchedule: {
            create: scheduleData,
          },
        },
        include: {
          tag_id: true,
          TourSchedule: { include: { TourScheduleDetail: true } },
          vehicle_id: true,
        },
      });
      return data;
    } catch (error) {
      console.log('_______________________________________________________');
      console.log(error);
      console.log('_______________________________________________________');
      if (error.code == 'P2025') {
        throw new ResponseException({
          status: HttpStatus.BAD_REQUEST,
          error,
          message:
            'Please check your vehicle, Tag ID or location type is of valid values',
        });
      }
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: error,
      });
    }
  }

  async updateTourImages(tourId: string, images: Array<string>): Promise<Tour> {
    const updatedTour = await this.prisma.tour.update({
      where: {
        id: tourId,
      },
      data: {
        tour_images: images,
      },
    });
    return updatedTour;
  }

  async getAllByUserId(
    getAllTourDTO: GetAllTourDTO,
    providerUserID: string,
  ): Promise<Tour[]> {
    const { search, limit, page, orderBy } = getAllTourDTO;
    let prismaOrderBy: Prisma.TourOrderByWithRelationInput | undefined =
      undefined;
    if (orderBy) {
      const [field, order] = orderBy.split(':');
      console.log(field, '-', order);
      prismaOrderBy = { [field]: order as 'asc' | 'desc' };
    }
    const where: Prisma.TourWhereInput = search
      ? {
          provider_id: providerUserID,
          AND: [
            { name: { contains: search, mode: 'insensitive' } },
            { description: { contains: search, mode: 'insensitive' } },
          ],
        }
      : {};

    const skip = page ? (page - 1) * (limit || 10) : undefined;
    const take = limit || 10;

    const tourData = await this.prisma.tour.findMany({
      include: {
        Provider: { include: { User: true } },
        TourAvailability: true,
        TourReview: { include: { ReviewReplies: true } },
        TourSchedule: { include: { TourScheduleDetail: true } },
        vehicle_id: true,
        tag_id: true,
        TicketPricing: { include: { PricingType: true, Ticket: true } },
      },
      skip: skip,
      take: take,
      where: where,
      orderBy: prismaOrderBy,
    });

    return tourData;
  }
  catch(error) {
    console.log(error);
    throw new ResponseException({
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      error: error,
    });
  }
}
