import { HttpStatus, Injectable } from '@nestjs/common';
import { AvailabilityStatusEnum, TourAvailability } from '@prisma/client';

import { TourAvailabilityDTO } from 'src/availability/dto/create-tour-availability.dto';
import { ResponseException } from 'src/common/interface/exception.interface';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class AvailabilityAccessService {
  constructor(private prisma: PrismaService) {}
  async createOne(dto: TourAvailabilityDTO): Promise<any> {
    try {
      let specialDate;
      if (dto.specialDates) {
        specialDate = dto.specialDates.map(element => {
          return JSON.parse(JSON.stringify(element));
        });
      }
      const weekDays = dto.weekdays.map(element => {
        return JSON.parse(JSON.stringify(element));
      });
      const isoDateFrom = new Date(dto.validity_date_range_from).toISOString();
      const isoDateTo = new Date(dto.validity_date_range_to).toISOString();
      const availability = this.prisma.tourAvailability.create({
        data: {
          name: dto.name,
          special_dates: specialDate,
          weekdays: weekDays,
          validity_date_range_from: isoDateFrom,
          validity_date_range_to: isoDateTo,
          tour_id: dto.tour_id,
        },
      });

      return availability;
    } catch (error) {
      console.log(error);
      if (error.message === 'provider') {
        throw new ResponseException({
          status: HttpStatus.BAD_REQUEST,
          message: 'Please complete provider signup before creating data',
          error,
        });
      } else {
        throw new ResponseException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error,
        });
      }
    }

    // throw new InternalServerErrorException(error)
  }
  async getOneById(id: number): Promise<TourAvailability> {
    try {
      const avail = await this.prisma.tourAvailability.findFirst({
        where: {
          AND: {
            id: id,
          },
        },
      });
      return avail;
    } catch (error) {
      console.log(error);
      if (error.message === 'provider') {
        throw new ResponseException({
          status: HttpStatus.BAD_REQUEST,
          message: 'Please complete provider signup before creating data',
          error,
        });
      } else {
        throw new ResponseException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error,
        });
      }
    }
  }

  async getOneByIdWithProvider(id: number): Promise<TourAvailability> {
    try {
      const avail = await this.prisma.tourAvailability.findFirst({
        where: {
          AND: {
            id: id,
          },
        },
        include: {
          Tour: { include: { Provider: true } },
        },
      });
      return avail;
    } catch (error) {
      console.log(error);
      if (error.message === 'provider') {
        throw new ResponseException({
          status: HttpStatus.BAD_REQUEST,
          message: 'Please complete provider signup before creating data',
          error,
        });
      } else {
        throw new ResponseException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error,
        });
      }
    }
  }
  async getAllByProviderId(
    UID: string,
    status?: AvailabilityStatusEnum,
    tourId?: string,
  ): Promise<TourAvailability[]> {
    try {
      console.log(UID);
      const avail = await this.prisma.tourAvailability.findMany({
        where: {
          AND: {
            status: status,
            tour_id: tourId,
          },
        },
      });
      return avail;
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: error,
      });
    }
  }

  async patchStatusAvailability(
    availId: number,
    status: AvailabilityStatusEnum,
  ): Promise<TourAvailability> {
    try {
      const avail = await this.prisma.tourAvailability.update({
        where: {
          id: availId,
        },
        data: {
          status: status,
        },
      });
      return avail;
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: error,
      });
    }
  }
  async patchOneById(
    id: number,
    status: AvailabilityStatusEnum,
  ): Promise<TourAvailability> {
    console.log(id);
    console.log(status);
    return null;
  }
  async deleteOneById(id: number) {
    try {
      return await this.prisma.tourAvailability.delete({ where: { id: id } });
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: error,
      });
    }
  }

  async updateOnById(dto: TourAvailabilityDTO, availId: number): Promise<any> {
    try {
      let specialDate;
      if (dto.specialDates) {
        specialDate = dto.specialDates.map(element => {
          return JSON.parse(JSON.stringify(element));
        });
      }
      const weekDays = dto.weekdays.map(element => {
        return JSON.parse(JSON.stringify(element));
      });
      const isoDateFrom = new Date(dto.validity_date_range_from).toISOString();
      const isoDateTo = new Date(dto.validity_date_range_to).toISOString();
      const availability = this.prisma.tourAvailability.update({
        data: {
          name: dto.name,
          special_dates: specialDate,
          weekdays: weekDays,
          validity_date_range_from: isoDateFrom,
          validity_date_range_to: isoDateTo,
        },
        where: {
          id: availId,
        },
      });

      return availability;
    } catch (error) {
      console.log(error);
      if (error.message === 'provider') {
        throw new ResponseException({
          status: HttpStatus.BAD_REQUEST,
          message: 'Please complete provider signup before creating data',
          error,
        });
      } else {
        throw new ResponseException({
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error,
        });
      }
    }

    // throw new InternalServerErrorException(error)
  }
}
