import { Module } from '@nestjs/common';
import { UtilitiesModule } from '../utilities/utilities.module';
import { DataAccessModule } from '../data-access/data-access.module';
import { AvailabilityController } from './availability.controller';
import { AvailabilityAccessService } from '../data-access/availability-access.service';
import { TimeService } from '../utilities/time.service';
import { ProviderDataAccess } from '../data-access/provider-access.service';
import { ValidationService } from '../utilities/validation.service';
import { TourAccessService } from '../data-access/tour-access.service';
import { AvailabilityService } from './availability.service';

@Module({
  controllers: [AvailabilityController],
  providers: [
    AvailabilityService,
    TourAccessService,
    AvailabilityAccessService,
    ProviderDataAccess,
    TimeService,
    ValidationService,
  ],
  imports: [UtilitiesModule, DataAccessModule],
})
export class AvailabilityModule {}
