import { HttpStatus, Injectable } from '@nestjs/common';
import { AvailabilityStatusEnum } from '@prisma/client';

import { ResponseException } from 'src/common/interface/exception.interface';
import { AvailabilityAccessService } from '../data-access/availability-access.service';
import { ValidationService } from '../utilities/validation.service';
import { TimeService } from './../utilities/time.service';
import { TourAvailabilityDTO } from './dto/create-tour-availability.dto';

@Injectable()
export class AvailabilityService {
  constructor(
    private availabilityAccessService: AvailabilityAccessService,
    private timeService: TimeService,
    private validationService: ValidationService,
  ) {}

  async createOneAvailability(dto: TourAvailabilityDTO, puid: string) {
    // if the provider has this tour or not, and if the provider is validated
    if (!dto.specialDates && !dto.weekdays) {
      throw new ResponseException({
        errorCode: 'AV04',
        status: HttpStatus.BAD_REQUEST,
        message: `Availability must has either specialdate or weekday`,
      });
    }
    const provider = await this.validationService.isProviderCreatedOrThrow(
      puid,
    );
    await this.validationService.doesProviderHasTourOrThrow(dto.tour_id, puid);
    await this.isValidityTimeRangeValid(
      dto.validity_date_range_from,
      dto.validity_date_range_to,
      provider.id,
      dto.tour_id,
    );
    // const availabilities =
    //   await this.availabilityAccessService.getAllByProviderId(
    //     provider.id,
    //     'ACTIVE',
    //     dto.tour_id,
    //   );

    // // validate validity time range
    // if (
    //   !this.timeService.validateAvailabilityTimeSpan(
    //     dto.validity_date_range_from,
    //     dto.validity_date_range_to,
    //   )
    // ) {
    //   throw new HttpException(
    //     {
    //       error: `Please enter correct validity date range ("from" date must be after both current day and the "to" date)`,
    //     },
    //     HttpStatus.BAD_REQUEST,
    //   );
    // }
    // //validate if the created validity times range collides with the others created
    // //Sort the array by availability date range
    // availabilities.sort((a, b) => {
    //   if (a.validity_date_range_from < b.validity_date_range_from) return -1;
    //   if (a.validity_date_range_from > b.validity_date_range_from) return 1;
    //   return 0;
    // });

    // const fromDate = new Date(dto.validity_date_range_from);
    // const toDate = new Date(dto.validity_date_range_to);
    // for (let index = 0; index < availabilities.length; index++) {
    //   const element = availabilities[index];

    //   if (
    //     fromDate < element.validity_date_range_from &&
    //     index == 0 &&
    //     toDate < element.validity_date_range_from
    //   ) {
    //     break;
    //   } else if (
    //     index === availabilities.length - 1 &&
    //     fromDate > element.validity_date_range_to
    //   ) {
    //     break;
    //   } else if (
    //     fromDate <= element.validity_date_range_to ||
    //     index === availabilities.length - 1
    //   ) {
    //     throw new HttpException(
    //       {
    //         error: `Validity date range must not conflict with the other availability, conflicted availability id ${element.id} of range ${element.validity_date_range_from}:${element.validity_date_range_to}`,
    //       },
    //       HttpStatus.BAD_REQUEST,
    //     );
    //   } else if (toDate < availabilities[index + 1].validity_date_range_from) {
    //     break;
    //   }
    // }

    if (dto.specialDates) {
      if (dto.specialDates.length > 365) {
        throw new ResponseException({
          errorCode: 'AV05',
          status: HttpStatus.BAD_REQUEST,
          message: `Special dates must not exceed 365 dates`,
        });
      }
      // validate time for each special dates
      const uniqueDateSet = new Set<Date>();
      dto.specialDates.forEach(element => {
        //validate unique dates for special dates
        if (uniqueDateSet.has(element.date)) {
          throw new ResponseException({
            errorCode: 'AV06',
            status: HttpStatus.BAD_REQUEST,
            message: 'Each Date of special dates must be unique',
          });
        }
        uniqueDateSet.add(element.date);

        if (
          !this.timeService.isTimeBetween(
            dto.validity_date_range_from,
            dto.validity_date_range_to,
            element.date,
          )
        ) {
          throw new ResponseException({
            errorCode: 'AV07',
            status: HttpStatus.BAD_REQUEST,
            message: `Each Date of special dates must be in between the validity date range:${element.date}`,
          });
        }
      });
    }

    if (dto.weekdays) {
      const uniqueWeekdaySet = new Set<number>();
      dto.weekdays.forEach(element => {
        console.log('cai nay co chay');
        if (uniqueWeekdaySet.has(element.day)) {
          throw new ResponseException({
            errorCode: 'AV08',
            status: HttpStatus.BAD_REQUEST,
            message: 'Each value of Day in weekdays must be unique',
          });
        }
        uniqueWeekdaySet.add(element.day);
        console.log(element.day);
      });
    }
    return this.availabilityAccessService.createOne(dto);
  }

  async getAvailabilityById(id: number, puid: string) {
    await this.validationService.isProviderCreatedOrThrow(puid);
    return await this.availabilityAccessService.getOneById(id);
  }

  async getAllByProviderId(
    uid: string,
    status?: AvailabilityStatusEnum,
    tourId?: string,
  ) {
    const provider = await this.validationService.isProviderCreatedOrThrow(uid);
    const result = await this.availabilityAccessService.getAllByProviderId(
      provider.id,
      status,
      tourId,
    );
    return result;
  }

  async deleteAvailability(uid: string, availId: number) {
    await this.validationService.doesProviderHasAvailabilityOrThrow(
      uid,
      availId,
    );

    const result = await this.availabilityAccessService.deleteOneById(availId);
    return result;
  }
  async deactivateAvailability(uid: string, availId: number) {
    await this.validationService.doesProviderHasAvailabilityOrThrow(
      uid,
      availId,
    );

    const result = await this.availabilityAccessService.patchStatusAvailability(
      availId,
      AvailabilityStatusEnum.INACTIVE,
    );
    return result;
  }

  async activateAvailability(uid: string, availId: number) {
    const data =
      await this.validationService.doesProviderHasAvailabilityOrThrow(
        uid,
        availId,
      );
    const availData = await this.availabilityAccessService.getOneById(availId);
    if (availData.status === 'ACTIVE') {
      throw new ResponseException({
        errorCode: 'AV03',
        status: HttpStatus.BAD_REQUEST,
        message: `This availability is already activated`,
      });
    }
    if (
      await this.isValidityTimeRangeValid(
        availData.validity_date_range_from,
        availData.validity_date_range_to,
        data.provider.id,
        availData.tour_id,
      )
    ) {
      return await this.availabilityAccessService.patchStatusAvailability(
        availId,
        AvailabilityStatusEnum.ACTIVE,
      );
    } else {
      throw new ResponseException({
        errorCode: 'AV02',
        status: HttpStatus.BAD_REQUEST,
        message: `Error activating availability, please review the validity time range`,
      });
    }
  }
  async isValidityTimeRangeValid(
    valid_from: Date,
    valid_to: Date,
    pid: string,
    tourId: string,
  ) {
    const availabilities =
      await this.availabilityAccessService.getAllByProviderId(
        pid,
        'ACTIVE',
        tourId,
      );

    // validate validity time range
    if (!this.timeService.validateAvailabilityTimeSpan(valid_from, valid_to)) {
      throw new ResponseException({
        errorCode: 'AV01',
        status: HttpStatus.BAD_REQUEST,
        message: `Please enter correct validity date range ("from" date must be after both current day and the "to" date)`,
      });
    }
    //validate if the created validity times range collides with the others created
    //Sort the array by availability date range
    availabilities.sort((a, b) => {
      if (a.validity_date_range_from < b.validity_date_range_from) return -1;
      if (a.validity_date_range_from > b.validity_date_range_from) return 1;
      return 0;
    });

    const fromDate = new Date(valid_from);
    const toDate = new Date(valid_to);
    for (let index = 0; index < availabilities.length; index++) {
      const element = availabilities[index];

      if (
        fromDate < element.validity_date_range_from &&
        index == 0 &&
        toDate < element.validity_date_range_from
      ) {
        return true;
      } else if (
        index === availabilities.length - 1 &&
        fromDate > element.validity_date_range_to
      ) {
        return true;
      } else if (
        fromDate <= element.validity_date_range_to ||
        index === availabilities.length - 1
      ) {
        throw new ResponseException({
          errorCode: 'AV02',
          status: HttpStatus.BAD_REQUEST,
          message: `Validity date range must not conflict with the other availability, conflicted availability id ${element.id} of range ${element.validity_date_range_from}:${element.validity_date_range_to}`,
        });
      } else if (toDate < availabilities[index + 1].validity_date_range_from) {
        return true;
      }
    }
    return true;
  }
  async updateOneAvailabilityById(
    dto: TourAvailabilityDTO,
    availId: number,
    puid: string,
  ) {
    // if the provider has this tour or not, and if the provider is validated
    if (!dto.specialDates && !dto.weekdays) {
      throw new ResponseException({
        errorCode: 'AV04',
        status: HttpStatus.BAD_REQUEST,
        message: `Availability must has either specialdate or weekday`,
      });
    }
    const provider = await this.validationService.isProviderCreatedOrThrow(
      puid,
    );
    await this.validationService.doesProviderHasTourOrThrow(dto.tour_id, puid);
    await this.isValidityTimeRangeValid(
      dto.validity_date_range_from,
      dto.validity_date_range_to,
      provider.id,
      dto.tour_id,
    );

    if (dto.specialDates) {
      if (dto.specialDates.length > 365) {
        throw new ResponseException({
          errorCode: 'AV05',
          status: HttpStatus.BAD_REQUEST,
          message: `Special dates must not exceed 365 dates`,
        });
      }
      // validate time for each special dates
      const uniqueDateSet = new Set<Date>();
      dto.specialDates.forEach(element => {
        //validate unique dates for special dates
        if (uniqueDateSet.has(element.date)) {
          throw new ResponseException({
            errorCode: 'AV06',
            status: HttpStatus.BAD_REQUEST,
            message: 'Each Date of special dates must be unique',
          });
        }
        uniqueDateSet.add(element.date);

        if (
          !this.timeService.isTimeBetween(
            dto.validity_date_range_from,
            dto.validity_date_range_to,
            element.date,
          )
        ) {
          throw new ResponseException({
            errorCode: 'AV07',
            status: HttpStatus.BAD_REQUEST,
            message: `Each Date of special dates must be in between the validity date range:${element.date}`,
          });
        }
      });
    }

    if (dto.weekdays) {
      const uniqueWeekdaySet = new Set<number>();
      dto.weekdays.forEach(element => {
        console.log('cai nay co chay');
        if (uniqueWeekdaySet.has(element.day)) {
          throw new ResponseException({
            errorCode: 'AV08',
            status: HttpStatus.BAD_REQUEST,
            message: 'Each value of Day in weekdays must be unique',
          });
        }
        uniqueWeekdaySet.add(element.day);
        console.log(element.day);
      });
    }
    return this.availabilityAccessService.updateOnById(dto, availId);
  }
}
