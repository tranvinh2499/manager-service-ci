export type Tour = {
  id: string;
  provider_user_id: string;
  name: string;
  description: string;
  footnote: string;
  tour_images: string[];
  duration_day: number;
  duration_night: number;
  location: string;
  status: string;
  created_At: string;
  updated_At: string;
};
