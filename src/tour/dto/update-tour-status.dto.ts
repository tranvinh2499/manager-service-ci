import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { TourStatus } from '../../common/models/tour';

export class UpdateTourStatusDTO {
  @IsString()
  @IsEnum(TourStatus)
  @ApiProperty({
    required: true,
  })
  status: string;
}
