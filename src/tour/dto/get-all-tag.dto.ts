import { IsString, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class GetAllTagDto {
  @ApiPropertyOptional()
  @IsOptional()
  @Type(() => Number)
  tag_type?: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search?: string;
}
