import {
  Body,
  Controller,
  Get,
  Param,
  ParseUUIDPipe,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { ProviderEnum } from '@prisma/client';
import { Request } from 'express';
import { Roles } from 'src/common/decorators/roles.decorator';
import { AtGuard } from 'src/common/guards';
import { RolesGuard } from 'src/common/guards/role.guard';
import { UpdateProviderStatusDto } from './dto/update-provider.dto';
import { StaffService } from './staff.service';

@ApiTags('Staff')
@Controller('staff')
@Roles(4, 2)
@UseGuards(AtGuard, RolesGuard)
export class StaffController {
  constructor(private readonly staffDataService: StaffService) {}

  @Get('provider-management/providers')
  @ApiQuery({ name: 'page', required: false })
  @ApiQuery({ name: 'select', required: false })
  @ApiQuery({ name: 'filter', required: false, enum: ProviderEnum })
  async getAllProviders(
    @Query('page') page = 1,
    @Query('select') select = 10,
    @Query('status') status: ProviderEnum,
  ) {
    return this.staffDataService.getAllProviders(page, select, status);
  }

  @Get('provider-management/providers/:id')
  getProviderDetail(@Param('id', ParseUUIDPipe) providerId: string) {
    return this.staffDataService.getProviderDetail(providerId);
  }

  @Put('provider-management/providers/:id')
  setStatusProvider(
    @Req() req: Request,
    @Body() dto: UpdateProviderStatusDto,
    @Param('id', ParseUUIDPipe) providerId: string,
  ) {
    return this.staffDataService.setStatusProvider(
      req.user.id,
      providerId,
      dto.status,
    );
  }
}
