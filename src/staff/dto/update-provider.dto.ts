import { ApiProperty } from '@nestjs/swagger';
import { ProviderEnum } from '@prisma/client';
import { IsEnum, IsOptional } from 'class-validator';

export class UpdateProviderStatusDto {
  @IsOptional()
  @ApiProperty()
  @IsEnum(ProviderEnum)
  status: string;
}
