import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient {
  constructor(configService: ConfigService) {
    super({
      datasources: {
        db: {
          url: configService.get('PROD')
            ? configService.get('DATABASE_URL')
            : configService.get('DATABASE_LOCAL'),
        },
      },
    });
  }
}
