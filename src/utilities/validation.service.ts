import { ForbiddenException, HttpStatus, Injectable } from '@nestjs/common';
import { Provider, ProviderEnum, Tour, User } from '@prisma/client';
import { ResponseException } from 'src/common/interface/exception.interface';
import { BlockedAccountDataService } from 'src/data-access/blocked-account-access.service';
import { DataAccessService } from 'src/data-access/data-access.service';
import { OTP_RETRY_LIMIT, ROLE } from '../common/constants/enum';
import { BLOCK_MESSAGE } from '../common/constants/string';
import { AvailabilityAccessService } from '../data-access/availability-access.service';
import { ProviderDataAccess } from '../data-access/provider-access.service';
import { TourAccessService } from '../data-access/tour-access.service';

@Injectable()
export class ValidationService {
  constructor(
    private tourAccessService: TourAccessService,
    private providerDataAccess: ProviderDataAccess,
    private availabilityAccessService: AvailabilityAccessService,
    private blockedAccountDataService: BlockedAccountDataService,
    private readonly dataAccessService: DataAccessService,
  ) {}
  async isProviderCreatedOrThrow(userId: string): Promise<Provider> {
    const provider = await this.providerDataAccess.getProviderByUserId(userId);
    //todo: check status of provider to know if the provider is still being processed
    if (provider) {
      return provider;
    } else {
      throw new ResponseException({
        status: HttpStatus.BAD_REQUEST,
        message: 'Please complete provider signup before creating data',
        errorCode: 'PR01',
      });
    }
  }
  async isTourCreatedOrThrow(tourId: string): Promise<Tour> {
    const tour = await this.tourAccessService.getOne(tourId);
    //todo: check status of provider to know if the provider is still being processed
    if (tour) return tour;
    else {
      throw new ResponseException({
        status: HttpStatus.BAD_REQUEST,
        message: 'Unknown Tour',
        errorCode: 'TO01',
      });
    }
  }
  //checking provide user id
  async doesProviderHasTourOrThrow(
    tourId: string,
    puid: string,
  ): Promise<{ tour: Tour; provider: Provider }> {
    const [tour, provider] = await Promise.all([
      this.isTourCreatedOrThrow(tourId),
      this.isProviderCreatedOrThrow(puid),
    ]);
    if (tour.provider_id === provider.id) {
      return { tour, provider };
    }

    throw new ResponseException({
      status: HttpStatus.BAD_REQUEST,
      message: 'Provider does not own this tour',
      errorCode: 'PO02',
    });
  }

  async doesProviderHasAvailabilityOrThrow(
    pid: string,
    availabilityId: number,
  ) {
    const avail = await this.availabilityAccessService.getOneById(
      availabilityId,
    );
    if (avail) {
      console.log('cai nay no cug hcay');
      const data = await this.doesProviderHasTourOrThrow(avail.tour_id, pid);
      return data;
    } else {
      console.log('cai nay no chay');

      throw new ResponseException({
        status: HttpStatus.BAD_REQUEST,
        message: 'This resource does not exist',
        errorCode: 'SY01',
      });
    }
  }

  async isEmailBlockedOrThrow(email: string): Promise<void> {
    const blocked =
      await this.blockedAccountDataService.getBlockedAccountsByEmail(email);
    const filter = blocked.filter(
      o => o.otpCount > OTP_RETRY_LIMIT.MAXIMUM_OTP_RETRY_LIMIT,
    );
    if (filter.length > 0) throw new ForbiddenException(BLOCK_MESSAGE);
  }

  async isAdministratorOrThrow(userId: string): Promise<void> {
    const user = await this.dataAccessService.getUniqueRecord<User>('user', {
      id: userId,
    });
    if (user.role_id !== ROLE.ADMIN)
      throw new ForbiddenException('Mày ko phải admin. Cút ra ngoài');
  }

  async isProviderAcceptedOrThrow(uid: string): Promise<void> {
    //Todo check when sign in provider is accepted or not
    const provider = await this.dataAccessService.getUniqueRecord<Provider>(
      'provider',
      {
        user_id: uid,
      },
    );
    if (provider) {
      if (provider.status === ProviderEnum.BANNED)
        throw new ResponseException({
          status: HttpStatus.FORBIDDEN,
          message:
            'You are banned from this website, contact for more information',
        });
      if (provider.status === ProviderEnum.DISABLED)
        throw new ResponseException({
          status: HttpStatus.FORBIDDEN,
          message:
            'Your account has been disabled from this website, contact for more information',
        });
      if (provider.status === ProviderEnum.REJECT)
        throw new ResponseException({
          status: HttpStatus.FORBIDDEN,
          message:
            'Your provider information is invalid. Contact the admins for more information',
        });
      if (provider.status === ProviderEnum.PROCESSING)
        throw new ResponseException({
          status: HttpStatus.BAD_REQUEST,
          message:
            'Please wait for admins to check your information before continuing',
        });
    }
  }
}
