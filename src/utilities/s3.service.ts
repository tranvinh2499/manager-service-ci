import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  S3Client,
  PutObjectCommand,
  PutObjectCommandInput,
  PutObjectAclCommandOutput,
} from '@aws-sdk/client-s3';
import { ResponseException } from 'src/common/interface/exception.interface';

@Injectable()
export class S3Service {
  private logger = new Logger(S3Service.name);
  private region: string;
  private s3: S3Client;

  constructor(private readonly configService: ConfigService) {
    this.region =
      this.configService.getOrThrow<string>('AWS_S3_REGION') ||
      'ap-southeast-1';
    this.s3 = new S3Client({
      region: this.region,
      credentials: {
        accessKeyId: this.configService.getOrThrow<string>('AWS_ACCESS_KEY_ID'),
        secretAccessKey:
          this.configService.getOrThrow<string>('AWS_PRIVATE_KEY'),
      },
    });
  }

  async uploadFile(file: Express.Multer.File, key: string) {
    const bucket = this.configService.get('AWS_S3_BUCKET');

    const input: PutObjectCommandInput = {
      Body: file.buffer,
      Bucket: bucket,
      Key: key,
      ContentType: file.mimetype,
      ACL: 'public-read',
    };
    try {
      const response: PutObjectAclCommandOutput = await this.s3.send(
        new PutObjectCommand(input),
      );
      if (response.$metadata.httpStatusCode === 200) {
        return `https://${bucket}.s3.${this.region}.amazonaws.com/${key}`;
      }
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Image not saved to s3!',
      });
    } catch (error) {
      this.logger.error(`Cannot save file inside s3`);
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: `Cannot save file inside s3`,
      });
    }
  }
}
