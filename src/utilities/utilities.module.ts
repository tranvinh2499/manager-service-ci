import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { DataAccessModule } from '../data-access/data-access.module';

import { ConfigModule, ConfigService } from '@nestjs/config';
import { AvailabilityAccessService } from '../data-access/availability-access.service';
import { BlockedAccountDataService } from '../data-access/blocked-account-access.service';
import { ExcelService } from './excel.service';
import { FirebaseService } from './firebase.service';
import { MailService } from './mail.service';
import { S3Service } from './s3.service';
import { TimeService } from './time.service';
import { ValidationService } from './validation.service';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        transport: {
          host: configService.get('MAILER_HOST'),
          auth: {
            user: configService.get('MAILER_EMAIL'),
            pass: configService.get('MAILER_PASSWORD'),
          },
        },
      }),
      inject: [ConfigService],
    }),
    DataAccessModule,
    DataAccessModule,
  ],
  providers: [
    S3Service,
    ExcelService,
    FirebaseService,
    MailService,
    TimeService,
    ValidationService,
    AvailabilityAccessService,
    BlockedAccountDataService,
  ],
  exports: [
    S3Service,
    ExcelService,
    FirebaseService,
    MailService,
    TimeService,
    ValidationService,
  ],
})
export class UtilitiesModule {}
