// excel.service.ts
import { Injectable, NotFoundException } from '@nestjs/common';
import * as ExcelJS from 'exceljs';

interface InputData {
  index: number;
  name: string;
  description: string;
  footnote: string;
  price: number;
  duration_day: number;
  duration_night: number;
  location: string;
  id_tags: string;
  scheldule: string[];
  scheldule_description: string[];
}

interface TourComponent {
  title: string;
  description: string;
}

interface OutputData {
  index: number;
  name: string;
  description: string;
  footnote: string;
  price: number;
  duration_day: number;
  duration_night: number;
  location: string;
  id_tags: number[];
  TourComponent: TourComponent[];
}

@Injectable()
export class ExcelService {
  async validateExcel(buffer: Buffer): Promise<void> {
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(buffer);

    // Define your template structure here
    const expectedHeaders = [
      'index',
      'name	',
      'description	',
      'footnote	',
      'price	',
      'duration_day',
      'duration_night	',
      'location	',
      'id_tags',
      'scheldule	',
      'scheldule_description',
    ];

    const sheet = workbook.getWorksheet(1);

    const headerRow = sheet.getRow(1);
    const actualHeaders = headerRow.values as string[];

    // Filter out empty cells or extra columns
    const trimmedActualHeaders = actualHeaders.filter(header => !!header);

    if (!this.headersMatch(expectedHeaders, trimmedActualHeaders)) {
      throw new NotFoundException(
        'Uploaded Excel structure does not match the template.',
      );
    }
  }
  private headersMatch(expected: string[], actual: string[]): boolean {
    if (expected.length !== actual.length) {
      return false;
    }

    for (let i = 0; i < expected.length; i++) {
      if (expected[i] !== actual[i]) {
        return false;
      }
    }

    return true;
  }

  async excelToJson(buffer: Buffer) {
    await this.validateExcel(buffer);

    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(buffer);

    const sheet = workbook.getWorksheet(1);

    const headerRow = sheet.getRow(1);

    const columnHeaders = headerRow.values as string[];

    columnHeaders.shift();

    const finalResult = new Map();
    const formatArray: any[] = [];

    for (let rowNumber = 2; rowNumber <= sheet.rowCount; rowNumber++) {
      const rowData = {};

      columnHeaders.forEach((header, colNumber) => {
        const cell = sheet.getCell(rowNumber, colNumber + 1);
        rowData[header] = cell ? cell.value : null;
      });

      const index = rowData['index'];

      const scheldule = rowData['scheldule'];
      const scheldule_description = rowData['scheldule_description'];

      if (finalResult.has(index)) {
        const existingData = finalResult.get(index);
        existingData.scheldule.push(scheldule);
        existingData.scheldule_description.push(scheldule_description);
      } else {
        rowData['scheldule'] = [rowData['scheldule']];
        rowData['scheldule_description'] = [rowData['scheldule_description']];
        finalResult.set(rowData['index'], rowData);
      }
    }

    const array = Array.from(finalResult.values());

    array.forEach(object => {
      const format = convert(object);
      formatArray.push(format);
    });

    return formatArray;
  }
}
function convert(input: InputData): OutputData {
  const idTags: number[] = input.id_tags
    .split(',')
    .map(tag => parseInt(tag.trim()));

  const tourComponent: TourComponent[] = input.scheldule.map(
    (title, index) => ({
      title: title,
      description: input.scheldule_description[index],
    }),
  );

  return {
    index: input.index,
    name: input.name,
    description: input.description,
    footnote: input.footnote,
    price: input.price,
    duration_day: input.duration_day,
    duration_night: input.duration_night,
    location: input.location,
    id_tags: idTags,
    TourComponent: tourComponent,
  };
}
