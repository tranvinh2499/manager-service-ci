import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { User } from '@prisma/client';

@Injectable()
export class MailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly config: ConfigService,
  ) {}

  // public sendVerificationLink(email: string) {
  //   const payload: VerificationTokenPayload = { email };

  //   const token = this.jwtService.sign(payload, {
  //     secret: this.config.get('JWT_VERIFICATION_TOKEN_SECRET'),
  //     expiresIn: `${this.config.get(
  //       'JWT_VERIFICATION_TOKEN_EXPIRATION_TIME',
  //     )}s`,
  //   });

  //   const url = `${this.config.get('EMAIL_CONFIRMATION_URL')}?token=${token}`;

  //   const text = `Welcome to the application. To confirm the email address, click here: ${url}`;

  //   return this.mailerService.sendMail({
  //     to: email,
  //     subject: 'Email confirmation',
  //     text,
  //   });
  // }

  async sendEmailVerifyCode(user: User, token: string) {
    const url = `example.com/auth/confirm?token=${token}`;
    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Welcome to Nice App! Confirm your Email',
      template: './confirmation', // `.hbs` extension is appended automatically
      context: {
        name: user.full_name,
        url,
      },
    });
  }

  async sendMail(email: string, code: string) {
    const _text = `Welcome to the application. Here is your Otp code: ${code}`;

    return await this.mailerService.sendMail({
      to: email, // list of receivers
      from: 'tranvinh2499@gmail.com', // sender address
      subject: 'Otp', // Subject line
      text: _text,
    });
  }
}
