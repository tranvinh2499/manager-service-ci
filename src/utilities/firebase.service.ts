import { Bucket, Storage } from '@google-cloud/storage';
import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ResponseException } from 'src/common/interface/exception.interface';

@Injectable()
export class FirebaseService {
  private storage: Storage;
  private bucket: Bucket;

  constructor(private readonly config: ConfigService) {
    this.storage = new Storage({
      projectId: config.get('FIREBASE_PROJECT_ID'),
      credentials: {
        client_email: config.get('FIREBASE_CLIENT_EMAIL'),
        client_id: config.get('FIREBASE_CLIENT_ID'),
        private_key: config.get('FIREBASE_SECRET'),
      },
    });
    this.bucket = this.storage.bucket('trading-stuff-67e33.appspot.com');
  }

  async uploadFile(folder: string, file: Express.Multer.File) {
    console.log('this run');
    try {
      const key = `${folder}/${file.originalname}`;

      const fileUpload = this.bucket.file(key);

      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype,
        },
      });

      blobStream.on('error', error => {
        console.log(error);
        throw new Error('images was not saved to firebase');
      });

      // Resolve the public URL after the upload is finished
      return new Promise<string>((resolve, reject) => {
        blobStream.on('finish', async () => {
          const file = this.bucket.file(key);

          const neverExpireDate = new Date('9999-12-31T23:59:59.999Z');

          const publicUrl = await file.getSignedUrl({
            action: 'read',
            expires: neverExpireDate,
          });

          resolve(publicUrl[0]); // The getSignedUrl() returns an array, so we return the first URL
        });

        // Reject the promise on any stream errors
        blobStream.on('error', reject);

        blobStream.end(file.buffer);
      });
    } catch (error) {
      throw new ResponseException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error,
      });
    }
  }

  async getImage(folder: string, fileName: string): Promise<string> {
    const key = `${folder}/${fileName}`;

    const file = this.bucket.file(key);

    try {
      const neverExpireDate = new Date('9999-12-31T23:59:59.999Z');

      const publicUrl = await file.getSignedUrl({
        action: 'read',
        expires: neverExpireDate,
      });

      return publicUrl[0];
    } catch (error) {
      console.log(error);
      throw new ResponseException({
        status: HttpStatus.BAD_REQUEST,
        error,
        message: 'Failed to get the image from Firebase',
      });
    }
  }

  async uploadMultiImage(files: Array<Express.Multer.File>, id: string) {
    const uploadedUrls = [];

    for (const file of files) {
      try {
        const url = await this.uploadFile(id, file);
        uploadedUrls.push(url);
      } catch (error) {
        console.error(error);
      }
    }

    return uploadedUrls;
  }

  async deleteImage(folder: string, fileName: string) {
    const filePath = `${folder}/${fileName}`;
    const file = this.bucket.file(filePath);

    await file.delete();

    console.log(`File ${filePath} deleted successfully.`);
  }
}
