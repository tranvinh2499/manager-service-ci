import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsString } from 'class-validator';

export class UpdateProviderDTO {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
  })
  website_url: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
  })
  description: string;

  @ApiPropertyOptional()
  @IsString()
  phone: string;

  @IsString()
  @IsEmail()
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsEmail()
  email: string;

  @IsString()
  @ApiProperty({})
  address_name: string;

  @IsString()
  @ApiProperty({})
  address_ward: string;

  @IsString()
  @ApiProperty({})
  address_province: string;

  @IsString()
  @ApiProperty({})
  address_country: string;
}
