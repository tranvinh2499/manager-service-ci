import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  Matches,
} from 'class-validator';
import { ProviderServiceType } from '../../common/models/provider';

export class CreateProviderDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  taxCode: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/)
  phone: string;

  @ApiProperty()
  @IsEmail()
  email: string;

  @IsString()
  @ApiProperty({})
  address_name: string;

  @IsString()
  @ApiProperty({})
  address_district: string;

  @IsString()
  @ApiProperty({})
  address_ward: string;

  @IsString()
  @ApiProperty({})
  address_province: string;

  @IsString()
  @ApiProperty({})
  address_country: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  companyName: string;

  @IsEnum(ProviderServiceType)
  @IsNotEmpty()
  @ApiProperty({
    enum: ProviderServiceType,
  })
  serviceType: string;

  @IsOptional()
  @ApiPropertyOptional({ type: 'string', format: 'binary' })
  file: any;
}
