import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { GENDER } from '../../common/models/user';

export class UpdateUserInputDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  fullName?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  dob?: string;

  @ApiPropertyOptional()
  @IsEnum(GENDER)
  @IsOptional()
  gender?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  phoneNumber?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  avatarImageUrl?: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  bannerImageUrl?: string;
}
