import { Module } from '@nestjs/common';
import { ProviderService } from './provider.service';
import { ProviderController } from './provider.controller';
import { DataAccessModule } from '../data-access/data-access.module';
import { UtilitiesModule } from '../utilities/utilities.module';
import { ValidationService } from '../utilities/validation.service';

@Module({
  controllers: [ProviderController],
  providers: [ProviderService, ValidationService, ValidationService],
  imports: [DataAccessModule, UtilitiesModule],
})
export class ProviderModule {}
