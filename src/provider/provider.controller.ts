import {
  Body,
  Controller,
  FileTypeValidator,
  Get,
  HttpStatus,
  MaxFileSizeValidator,
  ParseFilePipe,
  Patch,
  Post,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ProviderService } from './provider.service';

import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { ROLE } from 'src/common/constants/enum';
import { GetCurrentUser } from 'src/common/decorators/get-current-user.decorator';
import { Roles } from '../common/decorators/roles.decorator';
import { AtGuard } from '../common/guards';
import { RolesGuard } from '../common/guards/role.guard';
import { CreateProviderDto } from './dto/create-provider.dto';
import { UpdateProviderDTO } from './dto/update-provider.dto';

const MAX_FILE_SIZE_IMAGE = 1024 * 1024 * 8;

const FILE_TYPE =
  /^image\/(gif|jpe?g|png)$|^application\/(pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document)$|^text\/plain$/i;

const IMAGE_FILE_TYPE = /(jpg|jpeg|png|webp)$/;
@Controller('provider')
@ApiTags('Provider')
@Roles(ROLE.PROVIDER)
@UseGuards(AtGuard, RolesGuard)
@ApiBearerAuth()
export class ProviderController {
  constructor(private readonly providerService: ProviderService) {}

  @Post()
  @UseInterceptors(FileInterceptor('business_license'))
  @UseGuards(RolesGuard)
  @Roles(3)
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: CreateProviderDto,
  })
  createProvider(
    @GetCurrentUser('id') userId: string,
    @Body() dto: CreateProviderDto,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE_IMAGE,
          }),
          new FileTypeValidator({
            fileType: FILE_TYPE,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
  ) {
    console.log(dto);
    return this.providerService.createProvider(userId, dto, file);
  }

  @Get('profile')
  getProfile(@GetCurrentUser('id') userId: string) {
    return this.providerService.getInformation(userId);
  }

  @Put()
  updateProvider(
    @GetCurrentUser('id') id: string,
    @Body() dto: UpdateProviderDTO,
  ) {
    return this.providerService.updateProvider(id, dto);
  }

  @Patch('/avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  async updateProviderAvatar(
    @GetCurrentUser('id') id: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE_IMAGE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    file: Express.Multer.File,
  ) {
    return {
      status: HttpStatus.OK,
      message: 'OK',
      data: await this.providerService.patchProviderAvatar(id, file),
    };
  }

  @Patch('/banner')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  async updateProviderBanner(
    @GetCurrentUser('id') id: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({
            maxSize: MAX_FILE_SIZE_IMAGE,
          }),
          new FileTypeValidator({
            fileType: IMAGE_FILE_TYPE,
          }),
        ],
      }),
    )
    avatar: Express.Multer.File,
  ) {
    return {
      status: HttpStatus.OK,
      message: 'OK',
      data: await this.providerService.patchProviderBanner(id, avatar),
    };
  }
}
