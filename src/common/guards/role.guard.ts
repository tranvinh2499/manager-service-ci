// guards/roles.guard.ts
import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<number[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requiredRoles) {
      // If the 'roles' metadata is not defined for the route or the controller, deny access with a custom message.
      throw new ForbiddenException('Access to this resource is forbidden.');
    }

    const request = context.switchToHttp().getRequest();
    const user = request.user; // Assuming that you attach the user object to the request during authentication.

    // console.log(user.role_id);
    if (!user || typeof user.role_id !== 'number') {
      // If the user is not authenticated or doesn't have a numeric roleId, deny access with a custom message.
      throw new ForbiddenException('Access to this resource is forbidden.');
    }

    // Check if the user's roleId matches any of the required numeric roles to access the route.
    if (!requiredRoles.includes(user.role_id)) {
      // If the user's roleId doesn't match any of the required roles, deny access with a custom message.
      throw new ForbiddenException('Access to this resource is forbidden.');
    }

    // If the user has the required role, allow access to the route.
    return true;
  }
}
