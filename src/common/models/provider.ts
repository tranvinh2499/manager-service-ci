export enum ProviderServiceType {
  DOMESTIC = 'Domestic',
  INTERNATIONAL = 'International',
}
export enum ProviderStatus {
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
  REJECT = 'REJECT',
}
