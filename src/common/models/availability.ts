import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsISO8601, IsString, Matches } from 'class-validator';

export enum Weekday {
  Sunday = 1,
  Monday = 2,
  Tuesday = 3,
  Wednesday = 4,
  Thursday = 5,
  Friday = 6,
  Saturday = 7,
}

export class SpecialDates {
  @ApiProperty()
  @IsISO8601()
  date: Date;

  @ApiProperty()
  @IsString()
  @Matches(/^([01]\d|2[0-3]):[0-5]\d$/)
  timeSlot: string;
}
export class Weekdays {
  @ApiProperty()
  @IsEnum(Weekday)
  day: Weekday;

  @ApiProperty()
  @IsString()
  @Matches(/^([01]\d|2[0-3]):[0-5]\d$/)
  timeSlot: string;
}

export enum AvailabilityStatusSystemEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}
