export type ResponseDto<T> = {
  status: number;
  message: string;
  total?: number;
  data?: T[] | T;
  error?: any;
  pagination?: {
    total_count?: number;
    per_page?: number;
    page?: number;
    page_count?: number;
  };
};
