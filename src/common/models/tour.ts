import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export enum TourLocationTypeEnum {
  INTERNATIONAL = 'INTERNATIONAL',
  DOMESTIC = 'DOMESTIC',
}
class TourScheduleDetail {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;
}
export class TourSchedule {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  title: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({ type: TourScheduleDetail })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => TourScheduleDetail)
  schedule_detail?: TourScheduleDetail[];
}

export enum TourOrderBy {
  NAME_ASC = 'name:asc',
  NAME_DESC = 'name:desc',
  PRICE_ASC = 'price:asc',
  PRICE_DESC = 'price:desc',
  // Add more enum values for other properties and sorting options as needed
}

export enum TourStatus {
  DRAFT = 'DRAFT',
  PUBLISHED = 'PUBLISHED',
  HIDDEN = 'HIDDEN',
}
