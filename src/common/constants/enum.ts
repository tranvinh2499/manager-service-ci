export enum ROLE {
  USER = 1,
  ADMIN = 2,
  PROVIDER = 3,
  STAFF = 4,
}
export enum BLOCKED_ACCOUNT_TYPE {
  USER_REGISTRATION = 'User registration',
  FORGOT_PASSWORD = 'Forgot password',
}
export enum OTP_RETRY_LIMIT {
  MAXIMUM_OTP_RETRY_LIMIT = 9,
}

export const WeekdayEnum = {
  monday: 'monday_available_time',
  tuesday: 'tuesday_available_time',
  wednesday: 'wednesday_available_time',
  thursday: 'thursday_available_time',
  friday: 'friday_available_time',
  saturday: 'saturday_available_time',
  sunday: 'sunday_available_time',
};
