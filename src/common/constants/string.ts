export const DEFAULT_AVATAR =
  'https://images.squarespace-cdn.com/content/v1/54b7b93ce4b0a3e130d5d232/1519987020970-8IQ7F6Z61LLBCX85A65S/icon.png?format=1000w';

export const BLOCK_ACCOUNT_OTP = (email: string) =>
  `Your account ${email} is added to blocked list due to sending too many otp request. Kindly, contact the Admin to unblock your account for registration.`;

export const BLOCK_MESSAGE = `Your email have been added to blocked list. Kindly, contact the Admin to unblock your`;

export const OTP_NOT_EXPIRED = (time: number) =>
  'Otp sent yet to expire in ' + time + ' seconds';
