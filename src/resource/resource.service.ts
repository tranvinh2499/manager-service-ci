import { GetAllVehicleDTO } from './dto/get-all-vehicle.dto';
import { Tag, Vehicle } from '@prisma/client';
import { GetAllTagDto } from '../tour/dto/get-all-tag.dto';
import { TagAccessService } from './../data-access/tag-access.service';
import { Injectable } from '@nestjs/common';
import { VehicleAccessService } from '../data-access/vehicle-access.service';

@Injectable()
export class ResourceService {
  constructor(
    private tagAccessService: TagAccessService,
    private vehicleAccessService: VehicleAccessService,
  ) {}
  async getAllTag(getAllTag: GetAllTagDto): Promise<Tag[]> {
    return this.tagAccessService.getAllTag(getAllTag);
  }
  async getAllVehicle(getAllVehicleDTO: GetAllVehicleDTO): Promise<Vehicle[]> {
    return this.vehicleAccessService.getAllVehicle(getAllVehicleDTO);
  }
}
