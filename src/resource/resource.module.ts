import { Module } from '@nestjs/common';
import { ResourceService } from './resource.service';
import { ResourceController } from './resource.controller';
import { TagAccessService } from '../data-access/tag-access.service';
import { VehicleAccessService } from '../data-access/vehicle-access.service';

@Module({
  controllers: [ResourceController],
  providers: [ResourceService, TagAccessService, VehicleAccessService],
})
export class ResourceModule {}
